# SIMP Minutes

Minutes for SIMP Council and Committee meetings.

---

Copyright © 2017, Onyx Point, Inc.  
All content licensed under the [Creative Commons Attribution 4.0 International License](LICENSE.txt) unless otherwise noted.
