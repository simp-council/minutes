# SIMP Council Meeting - 2017-03-09

## Agenda

  - Officers
  - Code commit and merge access
  - Code Review privileges & requirements
  - Contributor agreement
  - Council election

## Attendees

Council:
  - Judy Johnson
  - Kendall Moore
  - Lisa Umberger
  - Trevor Vaughan
  - Lucas Yamanishi

Others:
  - Dylan Cochran
  - Nick Miller
  - Amanda Arnold
  - Jay Stoner
  - Tyler Walker
  - Chris Tessmer


## Resolutions

  * *The following Security Officers are herby appointed, the responsibilites of which are enumerated below.*
    - *Security Officer: Trevor Vaughan*
    - *Deputy Security Officer: Dylan Cochran*
    - *Responsibilities:*
      - *Administrative privileges to perform scrubbing of any security or legal related data from all Community Assets, JIRA, the SIMP GitHub community, the SIMP GitLab group, and the Google Groups mailing lists.*
      - *Performing backups of the data so that it may be recovered.*
      - *Notification of the council of all actions taken by the security team.*
      - *Notification of the transgressor if appropriate so the behavior can be corrected.*
      - *All actions must be reported.*
      - *A review of the report must be made within a week.*
      - *Three strikes policy for security officer acting in their capacity.*
    - Votes:
      - Judy: Yea
      - Kendall: Yea
      - Lucas: Yea
      - Lisa: Yea
      - Trevor: Yea

  * *PR Approval Guidelines*
    - *"Approved" (or "Approval") means read through the code or run through a test, automated or otherwise.*
    - *The definitions of "trivial" and "non-trivial" are left to the discretion of the reviewer or merger.*
    - *Every non-trivial pull request should have two (2) Approvals.*
    - *Trivial changes can be pushed through but must be justifiable.*
    - *Reviewers should provide comments and descriptions of their Approval to provide context for large changes.*
    - *Changes should be reasonably tested.*
    - Votes:
      - Judy: Yea
      - Kendall: Yea
      - Lucas: Yea
      - Lisa: Yea
      - Trevor: Yea

  * *Infrastructure Officers and GitHub repository access*
    - *The following are appointed as Infrastructure Officers*
      - *Trevor Vaughan*
      - *Dylan Cochran*
      - *Joshua Testerman*
    - *Infrastructure officers shall have full administrative access to all Community Assets*
    - *As it pertains to the SIMP GitHub community, the following roles are established:*
      - *"Owner" who shall have complete control (full administrative access)*
      - *"Developer" who shall receive review access and merge priviliges subject to the Approval Guidelines.*
    - *All rights are subject to the permissions model of the tools.*
    - *The Security Officer and Deputy Security Officer shall retain full administrative access to perform their duties.*
    - Votes:
      - Kendall: Yea
      - Lucas: Yea
      - Trevor: Yea
      - Judy: Yea
      - Lisa: Yea

  * *The following are herby granted SIMP Project Membership*
    - Lucas Yamanishi
    - Dylan Cochran
    - Nick Miller
    - Trevor Vaughan
    - Lisa Umberger
    - Amanda Arnold
    - Jay Stoner
    - Kendall Moore
    - Tyler Walker
    - Chris Tessmer
    - Judy Johnson
    - Josh Testerman

## Discussion

  * Security officer members and responsibilities
  * PR Approval Guidelines
  * Administrative access to GitHub and Infrastructure Officers.
    - Revoke administrative access for everybody to start.
  * Contributor Agreement
    - The intention is that every contributor agrees to license their contribution under our license, and agrees that they are not adding contributions that are legally 'clear' with regards to ownership and patents.
    - New GitHub Terms of Service are effectively Contributor License Agreement for contributions received through GitHub, but it does not cover patents, moral rights, and other areas of the law.  Moreover, it does not extend to contributions not received through GitHub.
      - https://help.github.com/articles/github-terms-of-service/
    - Potential agreements:
       - First draft based on Fedora: https://gitlab.com/simp-council/simp-governance/merge_requests/12
       - Harmony Agreements: http://harmonyagreements.org
    - Lucas to submit Harmony Agreements proposal via GitLab
    - Tabled
  * Council Elections
    - Lucas: Should we resign early and holding elections now?
    - Lisa: Let the Committees meet and get to work before spending time on this.
    - Tabled
  * Membership
    - Ask affiliates not in attendence.
    - Need to compile a better list of existing contributors.
    - Send announcement to simp-announce and others

## Schedule for next meeting:

  * CLA signing requirements.
  * Existing contributors list.
  * Elections tabled until subcommittee meetings have occured.
