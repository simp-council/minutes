# SIMP Council Meeting - 2017-02-27

## Attendees:

Councilmembers:

 * Judy Johnson
 * Kendall Moore
 * Lisa Umberger
 * Trevor Vaughan
 * Lucas Yamanishi

Others:

 * Jay Stoner
 * Chris Tessmer
 * Dylan Cochran
 * Nick Miller
 * Ryan Russell-Yates

## Resolutions

 * Initial vote to ratify Bylaws
    - https://gitlab.com/simp-council/simp-governance/merge_requests/16

 * Amendment to the Bylaws ensuring committees use procedures similar to the Council
    - https://gitlab.com/simp-council/simp-governance/merge_requests/20/diffs

 * Committee Charters:
    - Architecture & Planning Committee
        - https://gitlab.com/simp-council/simp-governance/merge_requests/17/diffs
    - Release Engineering Committee
        - https://gitlab.com/simp-council/simp-governance/merge_requests/18/diffs
    - Outreach Committee
        - https://gitlab.com/simp-council/simp-governance/merge_requests/19/diffs

 * *SIMP Puppet modules SHALL use the README file format recommended by Puppet, Inc.*
    - Judy Johnson: Yea
    - Kendall Moore: Absent
    - Lisa Umberger: Yea
    - Trevor Vaughan: Yea
    - Lucas Yamanishi: Yea

 * *The "Reference" section of SIMP Puppet modules MUST link to rendered puppet-strings pages or provide instructions for generating them,
   and MAY enumerate all resources and parameters.*
    - Judy Johnson: Yea
    - Kendall Moore: Absent
    - Lisa Umberger: Yea
    - Trevor Vaughan: Yea
    - Lucas Yamanishi: Yea

## Discussion

 * Bylaws
    - Honor system for anything that's not covered
        - Come to the table in good faith
        - Special Meetings should not be used to circumvent opposition
        - Councilmembers should post resolutions prior to meetings, if possible
        - We can institue stricter rules later if necessary
    - GitLab merge approvals can count as votes
    - We should prefer GitLab votes when possible
        - GitLab votes provide an automatic record
        - GitLab votes allow Councilmembers to cast their votes on their own schedules

 * Official README format
    - Most of our modules include YARD/puppet-strings documentation
    - Full puppet-strings documentation has been goal for a long time
    - There is concern that development on puppet-strings, a Puppet, Inc. project, is not progressing fast enough
    - Further concern that puppet-strings documentation does not look professional enough, specifically http://puppetmodule.info
    - In the future we could potentially host our own version of the *puppetmodule.info* site (https://github.com/domcleal/puppetmodule.info)
    - READMEs as stand-alone documents have value

## Action Items

 * Councilmembers to monitor GitLab

## Next Meeting (TBD)

 * Discuss and possibly resolve issues related to the following
	- Code commit and merge access
 	- Code Review privileges & requirements
	- Contributor agreement
	- Council elections
